module rle(
	//--- INPUTS ---//
	input	clk, // Clock signal that drives device
	input	nreset, // Initializes the RLE module
	input	start,// Tells RLE to start compressing the given frame
	input [31:0] message_addr, // Starting address of the plaintext frame
										// i.e., specifies from where RLE must read the plaintext frame
	input	[31:0] message_size,	// Length of the plain text in bytes
	input	[31:0] rle_addr, // Starting address of the ciphertext frame
								  // i.e., specifies where RLE must write the ciphertext frame
	input [31:0] port_A_data_out,	// read data from the dpsram (plaintext)

	//--- OUTPUTS ---//
	output reg [31:0] port_A_data_in, // write data to the dpsram (ciphertext)
	output reg [15:0] port_A_addr, // address of dpsram being read/written
	output port_A_clk, // clock to dpsram (drive this with the input clk)
	output reg port_A_we, // read/write selector for dpsram
	output reg [31:0] rle_size, // Length of the compressed text in bytes
	output reg done // done is a signal to indicate that encryption of the frame is complete
);
//--- STATE MANAGEMENT
parameter NONE = 0,
			 READ = 1,
			 COUNT = 2,
			 EOM = 3,
			 PREP_ONE = 4,
			 CHECK = 5,
			 WRITE = 6,
			 ALIGN = 7,
			 INCR = 8,
			 FINISH = 9;
reg [7:0] CURR_STATE = NONE;
//reg [4:0] NEXT_STATE = NONE;

//--- VARIABLES
reg [15:0] curr_r_addr;  // will act as a counter mechanism
reg [15:0] last_addr;
wire [15:0] next_addr;
wire [31:0] message_end;
wire [15:0] raw;
reg [31:0] holder;
reg [31:0] read_in;
reg write_state;

//--- ASSIGNMENTS
assign port_A_clk = clk; // we want the dpsram to have the clk
assign message_end = message_addr + message_size;
assign raw = curr_r_addr + 1;
assign next_addr = port_A_addr + 16'h4;

//--- FUNCTIONS

//--- COMBINATIONAL LOGIC

// CLOCKED
always@(posedge clk or negedge nreset) begin
	if(!nreset) begin		
		//outputs 
		port_A_addr    <= 0;
		port_A_data_in <= 0;
		port_A_we      <= 0;
		rle_size       <= 32'h0;
		done           <= 0;
		
		// reg
		curr_r_addr    <= 0;  // will act as a counter mechanism
		last_addr      <= 0;
		write_state    <= 0;
		
		// state
		CURR_STATE <= NONE; // waiting state
	end
	else if(start) begin
		//outputs 
		port_A_addr    <= message_addr[15:0];
		port_A_we      <= 0;
		rle_size       <= 0;
		done           <= 0;
		
		// reg
		curr_r_addr    <= message_addr[15:0];  // will act as a counter mechanism
		last_addr      <= rle_addr[15:0];
		write_state    <= 0;
		
		// state
		CURR_STATE <= READ;
	end
	else begin
		case(CURR_STATE)
			READ: begin
				read_in <= port_A_data_out; // read in the data from the DSRAM
				port_A_addr <= next_addr; // increment the address to the DSRAM	
				done <= 0;
				CURR_STATE <= INCR;
//				CURR_STATE <= COUNT;
			end //READ
			INCR: begin
//					port_A_data_in <= {read_in[7:0], 24'h01_0000};
					port_A_data_in[31:16] <= {read_in[7:0], 8'h1};
					port_A_we <= 0;
					done <= 0;
					if(raw % 4 == 0) begin
						read_in <= port_A_data_out; // read in the data from the DSRAM
						port_A_addr <= next_addr; // increment the address to the DSRAM
//						CURR_STATE <= FINISH;
					end
					else if(raw % 4) begin
						read_in <= read_in >> 8;
						curr_r_addr <= raw;
//						curr_r_addr <= raw;
					end
//					CURR_STATE <= COUNT;
						CURR_STATE <= (raw == message_end)? EOM : COUNT; 
			end
			COUNT: begin
				done <= 0;
//				if(port_A_data_in[23:16] == 0) begin
////					$display("Entering UPDATE COMPARE");
//					port_A_data_in[31:16] <= {read_in[7:0], 8'h1};
//
//					if(raw % 4 == 0) begin
//						read_in <= port_A_data_out; // read in the data from the DSRAM
//						port_A_addr <= next_addr; // increment the address to the DSRAM
////						$display("Updated read_in from update compare");
//					end
//					else if(raw % 4) begin
//						read_in <= read_in >> 8;
//					end
//
////					$display("Exiting UPDATE COMPARE");
//					CURR_STATE <= COUNT;
//				end
				if(port_A_data_in[31:24] == read_in[7:0]) begin // read the same character
					port_A_data_in[23:16] <= port_A_data_in[23:16] + 8'h1;
					
					if(raw % 4 == 0) begin
						read_in <= port_A_data_out; // read in the data from the DSRAM
						port_A_addr <= next_addr; // increment the address to the DSRAM
//						$display("Updated read_in from reading read_in");						
					end
					else if(raw % 4) begin
						read_in <= read_in >> 8;
					end
					
					// prepare to close 
					CURR_STATE <= (raw == message_end)? EOM : COUNT;
				end
				else if(port_A_data_in[31:24] != read_in[7:0]) begin
					$display("I'm in here");
					if(write_state == 0) begin 
						port_A_data_in <= {read_in[7:0], 8'h1, port_A_data_in[31:16]};
						write_state <= 1'h1;
						port_A_we <= 0;

						if(raw % 4 == 0) begin
							read_in <= port_A_data_out; // read in the data from the DPSRAM
							port_A_addr <= next_addr; // increment the address to the DSRAM			
//							$display("Updated read_in from making new compare");
						end
						else if(raw % 4) begin
							read_in <= read_in >> 8;				
						end
						CURR_STATE <= (raw == message_end)? EOM : COUNT; 
					end
					else if(write_state != 0) begin // write out to the DPSRAM
						write_state <= 0;
						port_A_we <= 1'h1;

						port_A_addr <= last_addr; // Swap out the address
					
						read_in <= read_in >> 8;
						holder <= {read_in[7:0], 24'h01_0000};

						if(raw % 4 == 0) begin // check to see if we're at the end of the WORD
							read_in <= port_A_data_out; // read in the data from the DPSRAM
							last_addr <= next_addr; // prepare to read the next line
//							$display("Updated read_in from inside WRITE");
						end
						else if(raw % 4) begin
							last_addr <= port_A_addr;
						end

						CURR_STATE <= WRITE;
					end
					rle_size <= rle_size + 32'h2;
				end
				else begin
					CURR_STATE <= EOM;
				end
				curr_r_addr <= raw;
			end //COUNT
			WRITE: begin // Process and increment
				port_A_addr <= last_addr; // go back to read address
				last_addr <= next_addr;
				port_A_we <= 1'h0; 					 // switch back to read mode
				port_A_data_in <= holder;
				done <= 0;
				// check to see if we are at the end of the message and exit if we must
				CURR_STATE <= (curr_r_addr == message_end)? EOM : COUNT; 
			end //WRITE
			EOM: begin
				port_A_data_in <= (write_state)? port_A_data_in : port_A_data_in >> 16;
				port_A_addr <= last_addr; // go back to read address
				last_addr <= port_A_addr;
				port_A_we <= 1'h1;
				done <= 0;
				rle_size <= rle_size + 32'h2;

				CURR_STATE <= FINISH;
			end //EOM
			FINISH: begin
				done <= 1'h1;
				port_A_data_in <= 0;
				port_A_we <= 0;
				CURR_STATE <= NONE;
			end //FINISH
			NONE: begin
				CURR_STATE <= NONE;
			end //NONE
			default: begin	
				CURR_STATE <= NONE;
			end //defaul
		endcase
	end
end
endmodule
