module rle(
	//--- INPUTS ---//
	input	clk, // Clock signal that drives device
	input	nreset, // Initializes the RLE module
	input	start,// Tells RLE to start compressing the given frame
	input [31:0] message_addr, // Starting address of the plaintext frame
										// i.e., specifies from where RLE must read the plaintext frame
	input	[31:0] message_size,	// Length of the plain text in bytes
	input	[31:0] rle_addr, // Starting address of the ciphertext frame
								  // i.e., specifies where RLE must write the ciphertext frame
	input [31:0] port_A_data_out,	// read data from the dpsram (plaintext)

	//--- OUTPUTS ---//
	output reg [31:0] port_A_data_in, // write data to the dpsram (ciphertext)
	output reg [15:0] port_A_addr, // address of dpsram being read/written
	output port_A_clk, // clock to dpsram (drive this with the input clk)
	output reg port_A_we, // read/write selector for dpsram
	output reg [31:0] rle_size, // Length of the compressed text in bytes
	output reg done // done is a signal to indicate that encryption of the frame is complete
);

//--- VARIABLES
reg [31:0] read_in; // the data that is read into the machine
//--- --- positional information
reg [15:0] last_addr; // place holder for the last position of r/w
reg [2:0] cmp_pos; // current location in the incoming bytes
wire [15:0] message_end;
reg [15:0] write_addr=0;// write address
reg [15:0] read_addr=0;//read address
reg [7:0] counter;//counter for the amount to be put in RLE
wire [15:0] extra;

assign extra= message_size%4;

assign message_end = message_addr + message_size;
//--- STATE MANAGEMENT
parameter NONE = 4'h0, 
			 INIT = 4'h1, 
			 READ = 4'h2, 
			 SETUP= 4'h3,
			 PROCESS = 4'h4, 
			 WRITE_CLEAN = 4'h7,
			 HALT = 4'h9;
reg [3:0] state = NONE;

reg [31:0] write_buffer=0;
reg write_side=0;
reg [2:0]read_sec=0;
reg ready_to_write = 0;
reg time_to_write=0;
reg [1:0] pos_finder = 0;
reg js=0;//just starting flag
reg [7:0] last_byte;//last byte read in

//--- ASSIGNMENTS
assign port_A_clk = clk; // we want the dpsram to have the clk
//--- FUNCTIONS
//--- COMBINATIONAL LOGIC
 
//--- SEQUENTIAL LOGIC
always@(posedge clk or negedge nreset) begin
	if(!nreset) begin
		port_A_data_in <= 32'h0;
		port_A_addr <= 16'h0;
		port_A_we <= 1'h0;
		cmp_pos <= 0;
		read_in <= 0;
		rle_size <= 32'h0;
		pos_finder <= 0;
		done <= 0;
		ready_to_write <= 0;
		
		state <= NONE;
	end
	else if(start) begin
		port_A_data_in <= 32'h0;
		port_A_addr <= message_addr[15:0];
		last_addr <= rle_addr[15:0];
		done <= 0;
		cmp_pos <= 0;
		//read_in <=message_addr[15:0];
		//port_A_data_in <= 0;
		rle_size <= 0;
		pos_finder <= 0;
		port_A_we<=0;
		ready_to_write <= 0;
		read_addr<= message_addr[15:0]+16'h4;
		read_in<=port_A_data_out;
		state <= INIT;
	end
	else begin
		case(state)
			NONE: begin
			end
			INIT: begin
				port_A_data_in[31:24] <= port_A_data_out[7:0];
				last_byte<=port_A_data_out[7:0];
				//last_byte<=1;
				read_sec<=1;
				counter<=8'h1;
				state <= READ;
				port_A_addr<=read_in;
				write_side<=0;
				time_to_write<=0;
				write_addr<=rle_addr[15:0];
				$display("read adder is at %d",read_addr);
				$display("read in at init is %h",read_in);
				js<=1;
			end
			READ: begin 
			$display("last byte is %h",last_byte);
				if(read_sec==0) begin
					if(read_in[7:0]==last_byte[7:0]&&!($signed(read_addr)>$signed(message_end+4+3))) begin
						if(read_in[15:8]==last_byte[7:0]&&!($signed(read_addr)>message_end+4+2)) begin
							if(read_in[23:16]==last_byte[7:0]&&!($signed(read_addr>message_end+4+1))) begin
								if(read_in[31:24]==last_byte[7:0]&&!($signed(read_addr)>message_end+4))
								begin
								//last byte is the same then read the next word in
								//don't worry about writing
									port_A_we<=0;
									port_A_addr<=read_addr;
									read_addr<=read_addr+4;
									$display("read sec 0 if 4th byte");
									$display("inital address is at %d",message_addr);
									$display("read address is at %d",read_addr);
									state<=SETUP;
									counter<=counter+4;
									
								end
								else if(!(read_addr>message_end+4))
								begin
								//4 byte else
								//in this case still read just  but also write to ciphertext
								 port_A_addr<=read_addr;
								 read_addr<=read_addr+4;
								 $display("read sec 0 else 4th byte");
								 state<=SETUP;
								 counter<=1;
								 port_A_we<=0;
								 last_byte<=read_in[31:24];
								 if(write_side==0) begin
									write_buffer[15:0]<={last_byte,counter+3};
									write_side<=1;
								 end
								 else begin
									time_to_write<=1;
									write_side<=0;
									write_addr<=write_addr;
									rle_size<=rle_size+4;
									$display("writing");
									write_buffer[31:16]<={last_byte,counter+3};				
								 end

								end
								else begin
								port_A_addr<=write_addr;
								port_A_we<=1;
								if(write_side==0)begin
									port_A_data_in<={write_buffer[31:16],last_byte,counter};
								end
								else begin
									port_A_data_in<={last_byte,counter,write_buffer[15:0]};
								end
								rle_size<=rle_size+4;
								state<=HALT;
								end
							end
							else if(!(read_addr>message_end+4+1)) begin
							//3 byte else
							//write to the ciphertext and then set read_sec= 3 
							read_sec<=3;
							last_byte<=read_in[23:16];
							counter<=1;	
							$display("read sec 0 else 3rd byte");
				  
							if(write_side==0) begin
								write_buffer[15:0]<={last_byte,counter+2};
								write_side<=1;
							end
							else begin
								port_A_we<=1;
								port_A_addr<=write_addr;
								write_addr<=write_addr+4;
								rle_size<=rle_size+4;
							   write_side<=0;
								write_buffer<=0;
								$display("writing");
								$display("write buffer is %h",{last_byte,counter+2,write_buffer[15:0]});
								port_A_data_in<={last_byte,counter+2,write_buffer[15:0]};
							end

							end
							else begin
							port_A_addr<=write_addr;
							port_A_we<=1;
							if(write_side==0)begin
								port_A_data_in<={write_buffer[31:16],last_byte,counter};
							end
							else begin
								port_A_data_in<={last_byte,counter,write_buffer[15:0]};
							end
							rle_size<=rle_size+4;
							state<=HALT;
							end
						end
						else if(!(read_addr>message_end+4+2)) begin
						//2 byte else
						//write to the ciphertext and then set read_sec= 2
							read_sec<=2;
							last_byte<=read_in[15:8];
							counter<=1;
							$display("read sec 0 else 2nd byte");
							if(write_side==0) begin
								$display("last byte counter %h%h",last_byte,counter[7:0]+1);
								write_buffer[15:0]<={last_byte,counter+1};
								write_side<=1;
							end
							else begin
								port_A_we<=1;
								port_A_addr<=write_addr;
								write_addr<=write_addr+4;
								rle_size<=rle_size+4;
								write_side<=0;
								write_buffer<=0;	
								$display("writing");	
								$display("write buffer is %h",{last_byte,counter+1,write_buffer[15:0]});
								port_A_data_in[31:24]<=last_byte;
								port_A_data_in[23:16]<=counter+1;
								port_A_data_in[15:0]<=write_buffer[15:0];
							end
						end
						else begin
						port_A_addr<=write_addr;
						port_A_we<=1;
						if(write_side==0)begin
							port_A_data_in<={write_buffer[31:16],last_byte,counter};
						end
						else begin
							port_A_data_in<={last_byte,counter,write_buffer[15:0]};
						end
						rle_size<=rle_size+4;
						state<=HALT;
						end
						
					end
					else if(!(read_addr>message_end+4+3)) begin
					//1 byte else
					//write to the ciphertext and then set read_sec= 1
						read_sec<=1;
						last_byte<=read_in[7:0];
						counter<=1;
						$display("read sec 0 else 1st byte");
						//$display("read sec is %h ",read_sec);
						//$display("last_byte is %h",last_byte);
						//$display("read in at that location is %h",read_in[7:0]);
						//$display("counter is at %h",counter);
						if(write_side==0) begin
							write_buffer[15:8]<=last_byte; 
							write_buffer[7:0]<=counter;
							write_side<=1;
						end
						else begin
							port_A_we<=1;
							port_A_addr<=write_addr;
							write_addr<=write_addr+4;
							write_side<=0;
							write_buffer<=0;
							rle_size<=rle_size+4;
							read_sec<=1;
							last_byte<=read_in[7:0];
							counter<=1;

							$display("writing");
							$display("write buffer is %h",{last_byte,counter,write_buffer[15:0]});
							port_A_data_in[31:24]<=last_byte;
							port_A_data_in[23:16]<=counter;
							port_A_data_in[15:0]<=write_buffer[15:0];
						end	
					end
					else begin
					 port_A_addr<=write_addr;
					 port_A_we<=1;
					 $display("wtf are you doing here you son ");
					 $display("message_addr-4-3==%d",$signed(message_addr-4-3));
					 if(write_side==0)begin
						 port_A_data_in<={write_buffer[31:16],last_byte,counter};
					 end
					 else begin
						port_A_data_in<={last_byte,counter,write_buffer[15:0]};
					 end
					 rle_size<=rle_size+4;
					 state<=HALT;
					
					end
				end
			   else if(read_sec==1) begin
				if(read_in[15:8]==last_byte[7:0]&&!(read_addr>message_end+4+2)) begin
					if(read_in[23:16]==last_byte[7:0]&&!(read_addr>message_end+4+1)) begin
						if(read_in[31:24]==last_byte[7:0]&&!(read_addr>message_end+4)) begin
						//last byte is the same then read the next word in
						//don't worry about writing
							port_A_we<=0;
							port_A_addr<=read_addr;
							$display("read sec 1 else 4th byte");
							read_addr<=read_addr+4;
							state<=SETUP;
							counter<=counter+3;

						end
						else if(!(read_addr>message_end+4)) begin
						//4 byte else
						//in this case just write to ciphertext then send to something to read
						 port_A_addr<=read_addr;
						 read_addr<=read_addr+4;
						 $display("read sec 1 else 4th byte");
						 state<=SETUP;
						 counter<=1;
						 port_A_we<=0;
						 last_byte<=read_in[31:24];
						 if(write_side==0) begin
							write_buffer[15:0]<={last_byte,counter+2};
							write_side<=1;
						 end
						 else begin
							time_to_write<=1;
							write_side<=0;
							write_addr<=write_addr;
							rle_size<=rle_size+4;
							$display("writing");
							write_buffer[31:16]<={last_byte,counter+2};				
						 end

						end
						else begin
						port_A_addr<=write_addr;
						port_A_we<=1;
						if(write_side==0)begin
							port_A_data_in<={write_buffer[31:16],last_byte,counter};
						end
						else begin
							port_A_data_in<={last_byte,counter,write_buffer[15:0]};
						end
						rle_size<=rle_size+4;
						state<=HALT;
						
						end
					end
					else if(!(read_addr>message_end+4+1)) begin
					//3 byte else
					//write to the ciphertext and then set read_sec= 3
						read_sec<=3;
						last_byte<=read_in[23:16];
						counter<=1;		
						$display("read_sec 1 else 3rd byte");
						if(write_side==0) begin
							write_buffer[15:0]<={last_byte,counter+1};
							write_side<=1;
						end
						else begin
							port_A_we<=1;
							port_A_addr<=write_addr;
							write_addr<=write_addr+4;
							rle_size<=rle_size+4;
							write_buffer<=0;
							write_side<=0;
							$display("writing");
							$display("write buffer is %h",{last_byte,counter+1,write_buffer[15:0]});
							port_A_data_in<={last_byte,counter+1,write_buffer[15:0]};
							
						end
					end
					else begin
					port_A_addr<=write_addr;
					port_A_we<=1;
					if(write_side==0)begin
					 port_A_data_in<={write_buffer[31:16],last_byte,counter};
					end
					else begin
					 port_A_data_in<={last_byte,counter,write_buffer[15:0]};
					end
					rle_size<=rle_size+4;
					state<=HALT;					
					end
				end
				else if(!(read_addr>message_end+4+2)) begin
				//2 byte else
				//write to the ciphertext and then set read_sec= 2
				  read_sec<=2;
				  last_byte<=read_in[15:8];
				  counter<=1;
				  $display("read sec 1 else 2nd byte");
				  $display("write address is %d",write_addr);
				 // $display("last_byte is %h",last_byte);
				 // $display("read in at the location is %h",read_in[15:8]);
				 // $display("all of read in is %h",read_in);
				  //$display("read_addr is %d",read_addr);
				  if(write_side==0) begin
					write_buffer[15:0]<={last_byte,counter};
					write_side<=1;
				  end
				  else begin
					port_A_we<=1;
					port_A_addr<=write_addr;
					write_addr<=write_addr+4;
					rle_size<=rle_size+4;
					write_side<=0;
					write_buffer<=0;					
					$display("writing");
					$display("write buffer is %h",{last_byte,counter,write_buffer[15:0]});
					port_A_data_in<={last_byte,counter,write_buffer[15:0]};
				  end

				end
				else begin
				 port_A_addr<=write_addr;
				 port_A_we<=1;
				 if(write_side==0)begin
					port_A_data_in<={write_buffer[31:16],last_byte,counter};
				 end
				 else begin
					port_A_data_in<={last_byte,counter,write_buffer[15:0]};
				 end
				 rle_size<=rle_size+4;
				 state<=HALT;					
				end
				
			
				
				
				
			   end
			   else if(read_sec==2)	begin
				if(read_in[23:16]==last_byte[7:0]&&!(read_addr>message_end+4+1)) begin
						if(read_in[31:24]==last_byte[7:0]&&!(read_addr>message_end+4)) begin
						//last byte is the same then read the next word in
						//don't worry about writing
						port_A_we<=0;
						port_A_addr<=read_addr;
						$display("read sec 2 if 4th byte");
						read_addr<=read_addr+4;
						state<=SETUP;
						counter<=counter+2;

						end
						else if(!(read_addr>=message_end+4)) begin
						//4 byte else
						//in this case just write to ciphertext then send to something to read
						 port_A_addr<=read_addr;
						 read_addr<=read_addr+4;
						 $display("read sec 2 else 4th byte");
						 state<=SETUP;
						 counter<=1;
						 port_A_we<=0;
						 last_byte<=read_in[31:24];
						 if(write_side==0) begin
							write_buffer[15:0]<={last_byte,counter+1};
							write_side<=1;
						 end
						 else begin
							time_to_write<=1;
							write_side<=0;
							write_addr<=write_addr;
							rle_size<=rle_size+4;
							$display("writing");
							write_buffer[31:16]<={last_byte,counter+1};				
						 end
						end
						else begin
						
						port_A_addr<=write_addr;
						port_A_we<=1;
						if(write_side==0)begin
							port_A_data_in<={write_buffer[31:16],last_byte,counter};
						end
						else begin
							port_A_data_in<={last_byte,counter,write_buffer[15:0]};
						end
						rle_size<=rle_size+4;
						state<=HALT;
						
						end
				 end
				 else if(!(read_addr>message_end+4+1)) begin
				  //3 byte else
				  //write to the ciphertext and then set read_sec= 3
				  read_sec<=3;
				  last_byte<=read_in[23:16];
				  counter<=1;
				  $display("read sec 2 else 3rd byte");
				  $display("writing address is this %d",write_addr);
				  if(write_side==0) begin
					write_buffer[15:0]<={last_byte,counter};
					write_side<=1;
				  end
				  else begin
					port_A_we<=1;
					port_A_addr<=write_addr;
					write_addr<=write_addr+4;
					write_side<=0;
					write_buffer<=0;
					rle_size<=rle_size+4;
					$display("writing");
					$display("write buffer is %h",{last_byte,counter,write_buffer[15:0]});
					port_A_data_in<={last_byte,counter,write_buffer[15:0]};
				  end
				   
				 end
				 else begin
				 port_A_addr<=write_addr;
				 port_A_we<=1;
				 if(write_side==0)begin
					port_A_data_in<={write_buffer[31:16],last_byte,counter};
				 end
				 else begin
					port_A_data_in<={last_byte,counter,write_buffer[15:0]};
				 end
				 rle_size<=rle_size+4;
				 state<=HALT;
				 
				 end
				end
				else if(read_sec==3) begin
				  if(read_in[31:24]==last_byte[7:0]&&!(read_addr>message_end+4))begin
				  //last byte is the same then read the next word in
				  //don't worry about writing
				  port_A_we<=0;
				  port_A_addr<=read_addr;
				  $display("read sec 3 if 4th byte");
				  read_addr<=read_addr+4;
				  state<=SETUP;
				  counter<=counter+1;
						
	   		  end
				  else if(!(read_addr>message_end+4)) begin
				  //4 byte else
				  //in this case just write to ciphertext then send to something to read
				  //state<=SETUP;
				  port_A_addr<=read_addr;
				  read_addr<=read_addr+4;
				  $display("read sec 3 else 4th byte");
				  state<=SETUP;
				  counter<=1;
				  port_A_we<=0;
				  last_byte<=read_in[31:24];
				  if(write_side==0) begin
					write_buffer[15:0]<={last_byte,counter};
					write_side<=1;
				  end
				  else begin
					time_to_write<=1;
					write_side<=0;
					write_addr<=write_addr;
					rle_size<=rle_size+4;
					$display("writing");
					$display("read_addr is at %d",read_addr);
					write_buffer[31:16]<={last_byte,counter};				
				  end
				  end	
				  else begin
				  port_A_addr<=write_addr;
				  port_A_we<=1;
				  if(write_side==0)begin
				  port_A_data_in<={write_buffer[31:16],last_byte,counter};
				  end
				  else begin
				  port_A_data_in<={last_byte,counter,write_buffer[15:0]};
				  end
				  rle_size<=rle_size+4;
				  state<=HALT;
				  end
				end
			end
			SETUP: begin
			read_in<=port_A_data_out;
			read_sec<=0;
			state<=READ;
			time_to_write<=0;
			$display("gets into setup ");
			$display("read in is now %h",port_A_data_out);
			//$display("read_addr is %d",read_addr);
			if(time_to_write==1) begin
				port_A_we<=1;
				port_A_addr<=write_addr;
				write_addr<=write_addr+4;
				//rle_size<=rle_size+4;
				port_A_data_in=write_buffer;
				$display("printing to buffer at setup");
				$display("print buffer is %h",write_buffer);
				write_buffer<=0;
			end
			if(read_addr-4-extra>message_end) begin
				$display("%d %d %d %d",read_addr,message_end,message_size,message_addr);
				$display("write buffer is this: %h",write_buffer);
				$display("write_side is %d",write_side);
				port_A_addr<=write_addr;
				$display("write addr is this %d",write_addr);
				if(write_side==0)begin
					port_A_data_in<={16'h0,last_byte,counter};
				end
				else begin
					$display("gets into here pls just once come on");
					port_A_data_in<={last_byte,counter,write_buffer[15:0]};
				end
				port_A_we<=1;
				rle_size<=rle_size+4;
				state<=HALT;
			end
							
			end
			HALT: begin
			$display("halt");
				port_A_we <= 0;
				done <= 1'h1;
			end

		endcase
	end
end

endmodule