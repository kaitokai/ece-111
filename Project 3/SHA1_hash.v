module SHA1_hash (       
	clk, 		
	nreset, 	
	start_hash,  
	message_addr,	
	message_size, 	
	hash, 	
	done, 		
	port_A_clk,
        port_A_data_in,
        port_A_data_out,
        port_A_addr,
        port_A_we
	);

input	clk;
input	nreset; 
// Initializes the SHA1_hash module

input	start_hash; 
// Tells SHA1_hash to start hashing the given frame

input 	[31:0] message_addr; 
// Starting address of the messagetext frame
// i.e., specifies from where SHA1_hash must read the messagetext frame

input	[31:0] message_size; 
// Length of the message in bytes

output	[159:0] hash; 
// hash results


input   [31:0] port_A_data_out; 
// read data from the dpsram (messagetext)

output  [31:0] port_A_data_in;
// write data to the dpsram (ciphertext)

output  [15:0] port_A_addr;
// address of dpsram being read/written 

output  port_A_clk;
// clock to dpsram (drive this with the input clk) 

output  port_A_we;
// read/write selector for dpsram

output	done; // done is a signal to indicate that hash  is complete
//registers
reg [31:0] h0,h1,h2,h3,h4,
				a, b, c, d, e,temp;
reg [2:0] state=4;
reg done;
assign hash= {h0,h1,h2,h3,h4};
reg [31:0] mes_size;
reg [31:0] port_A_data_in;
reg [15:0] port_A_addr;
reg port_A_we;
reg [15:0] read_addr;
reg [1:0] black_flag=0; //flag for appending the message size onto the end of the message
reg [31:0] w[0:15];
reg [6:0] t;
//reg [31:0] k=32'h5a827999;
//reg [1:0] tcounter=0;
parameter padding=0,
			 appending=1,
			 processing=2,
			 out=3,
			 start=4,
			 zeros=5,
			 read=6,
			 idle=7;
parameter tone=0,
			 ttwo=1,
			 tthree=2,
			 tfour=3;
wire[1:0] extra;
assign extra=message_size%4;
assign port_A_clk = clk;
wire [15:0] noExtra;
assign noExtra=message_size - extra;
wire [31:0] something;
assign something=w[(t-3)&32'd15]^w[(t-8)&32'd15]^w[(t-14)&32'd15]^w[(t-16)&32'd15];
wire [3:0] tMinusOne=(t-1)&32'd15;

wire [31:0] case1;
assign case1=((a&{b[1:0],b[31:2]})^((~a)&c));
wire [31:0] case2;
assign case2=(a^{b[1:0],b[31:2]}^c);
wire [31:0] case3;
assign case3=((a&{b[1:0],b[31:2]})^(a&c)^({b[1:0],b[31:2]}&c));
wire [31:0] case4;
assign case4=(a^{b[1:0],b[31:2]}^c);
wire [31:0] eh;
assign eh={a[26:0],a[31:27]}+temp+w[tMinusOne];
wire cin1;
wire cin2;
wire cin3;
wire cin4;
wire [31:0] test1;
wire [31:0] test2;
wire [31:0] test3;
wire [31:0] test4;
wire [31:0] out1;
wire [31:0] out2;
wire [31:0] out3;
wire [31:0] out4;
wire dd,gg,ff,qq;

	

function [31:0] changeEndian;   //transform data from the memory to big-endian form
    input [31:0] value;
    changeEndian = {value[7:0], value[15:8], value[23:16], value[31:24]};
endfunction
function [31:0] circularShift; //circularShift
	input [31:0] value;
	input [5:0] shifter;
	circularShift = (value<<shifter)|(value>>(32-shifter));
endfunction
//step 1 we need to find out where to put that intial 1 after the message is done.
//test=(message_size+1)%4
// if test==0 then we need to grab next line and add 1
// else shift a 1 into the place and or it with what is at the last line
// then increase message size 
always@(posedge clk or negedge nreset)begin
	if(!nreset) begin
		port_A_addr<=0;
		state<=idle;
	end
	else begin
		case(state)
			idle: begin
				if(start_hash)begin
					state<=start;
					port_A_addr<=(noExtra) + message_addr;
					port_A_we<=0;
				end
				else begin
					state<=idle;
				end
			end
			start: begin
				port_A_we<=0;
				// Read the last word of the intial message
				port_A_addr<=(noExtra) + message_addr;
				//$display("start");
//				//$display("first memory addres is %d",(message_size - extra) + message_addr);
//				//$display("extra is %d  message size is %d",extra,message_size);
//				//$display("message_addr is %d",message_addr);
//				//$display("start_hash is %d",start_hash);
				state <= (start_hash)?padding:start;
				h0<=32'h67452301;
				h1<=32'hefcdab89;
				h2<=32'h98badcfe;
				h3<=32'h10325476;
				h4<=32'hc3d2e1f0;
				read_addr<=message_addr;
				t<=0;
			end
			padding: begin
				port_A_we<=1;
				//$display("padding");
//				//$display("first memory addres is %d",(message_size - extra) + message_addr);
//				//$display("extra is %d  message size is %d",extra,message_size);
//				//$display("message_addr is %d",message_addr);
				if(extra==0)begin
					port_A_addr<=(message_size) + message_addr;
					port_A_data_in<=32'h80;
					mes_size<=message_size+4;
				end
				else begin
					$display("port_A_data_out %h",port_A_data_out);
					port_A_addr<=(noExtra) + message_addr; 
					port_A_data_in<= (port_A_data_out|(32'h80<<(extra*8)));
					mes_size<=noExtra+4;
				end
				state<=zeros;
			end
			zeros: begin
				//$display("zeros");
				//$display("number is %d",mes_size%64);
				if((mes_size)%64==56) begin
					state<=appending;
					port_A_addr<=port_A_addr+4;
					port_A_we<=1;
					port_A_data_in<=32'h0;
				end
				else begin
					state<=zeros;
					port_A_addr<=port_A_addr+4;
					port_A_we<=1;
					port_A_data_in<=32'h0;
					mes_size<=mes_size+4;
				end			
			end
			appending: begin
				port_A_we<=1;
				port_A_addr<=port_A_addr+4;
				port_A_data_in<=changeEndian(message_size*8);
				mes_size<=mes_size+4;
				state<=read;
			end
			read: begin

				port_A_addr<=read_addr;
				
				//$display("t is at this point %d read_addr is at %d",t,read_addr);
				if(port_A_we==1&&black_flag==0) begin
					port_A_we <= 0;
					black_flag<=1;
					read_addr<=read_addr+4;	
				end
				else if(black_flag==1)begin
					black_flag<=2;
					read_addr<=read_addr+4;		
					t<=0;
				end
				else if(t<=13)begin
					//$display("t place is %d",t);
					//$display("%h",port_A_data_out);
					w[t]<=changeEndian(port_A_data_out);
					t<=t+1;
					read_addr<=read_addr+4;
				end
				else if(t<=15) begin
					t<=t+1;
					w[t]<=changeEndian(port_A_data_out);
				end
				else if(t<80)begin
					w[t&32'd15]<={something[30:0],something[31]};//circularShift(w[(t-3)&32'd15]^w[(t-8)&32'd15]^w[(t-14)&32'd15]^w[(t-16)&32'd15],1);//circular shift by 1
					t<=t+1;
				end
				else begin
				//$display("does it get into here");
				state<=processing;
				end

			end
			processing:begin
				h0<=a+h0;
				h1<=b+h1;
				h2<=c+h2;
				h3<=d+h3;
				h4<=e+h4;
				read_addr<=read_addr+4;
				state<=out;
			end		
			out: begin 
				//hash<={h0,h1,h2,h3,h4};
				t<=0;
				port_A_addr<=read_addr;
				read_addr<=read_addr+4;
				black_flag<=0;
				if(mes_size==read_addr-message_addr-8)begin
					done<=1;
				end
				else begin
					state<=read;
				end
			end
			default: begin
			end
		endcase
		if(t>0&&t<=80) begin		
			$display("w[%d] is at this point is %h",t-2,w[(t-2)&32'd15]);
			e<=d;
			d<=c;
			c<={b[1:0],b[31:2]};
			b<=a;
			//$display("circle jerk is %h",circularShift(a,5));
			//$display("value of w[%d] is %h",t-1,w[(t-1)%16]);
			//$display("value of k is %h",k);
			//a<=(circularShift(a,5))+f+w[(t-1)>>5]+k+e;
			$display("its in the correct spot");
		
			$display("(b&c)^((~b)&d)= %h",(b&c)^((~b)&d));
			$display("w(t-1)= %h",w[(t-2)%16]);
			//$display("k = %h",k);
			$display("circularShift(a,5) %h",(circularShift(a,5)));
			if(t<20) begin
				$display("case one");
				$display("crazy value is %h",(a&{b[1:0],b[31:2]})^((~a)&c));
				temp<=32'h5a827999+d+case1;//((a&{b[1:0],b[31:2]})^((~a)&c));
				//temp<=out1;
			end
			else if(t<40) begin
				//temp<=out2;
				temp<=32'h6ed9eba1+d+case2;//(a^{b[1:0],b[31:2]}^c);
				//tcounter<=ttwo;
			end
			else if(t<60) begin
				//temp<=out3;
				temp<=32'h8f1bbcdc+d+case3;//((a&{b[1:0],b[31:2]})^(a&c)^({b[1:0],b[31:2]}&c));
			//	tcounter<=tthree;
			end
			else begin
				//temp<=out4;
				temp<=32'hca62c1d6+d+case4;//(a^{b[1:0],b[31:2]}^c);
				//tcounter<=tfour;
			end			
			a<=eh;	
			$display("value of t is %d",t);
			$display("value of a is %h",a);
			$display("value of b is %h",b);
			$display("value of c is %h",c);
			$display("value of d is %h",d);
			$display("value of e is %h",e);
			//$display("value of f is %h",f);
			//$display("tcounter is %d",tcounter);
			
			if(t==3)begin
				//$stop;
			end
			
		end
		else begin
		//tcounter<=tone;
		temp<=32'h5a827999+h4+((h1&h2)^((~h1)&h3));
		a<=h0;
		b<=h1;
		c<=h2;
		d<=h3;
		e<=h4;
		//f<=(b&c)^((~b)&d);
		end
	
	end
	end
endmodule